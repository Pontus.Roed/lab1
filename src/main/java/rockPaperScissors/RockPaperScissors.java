package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;
public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round "+ roundCounter);
        while (true){
            String human = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            Random random = new Random();
            String computer = rpsChoices.get(random.nextInt(rpsChoices.size()));
            if (human.equals("rock")){
                if (computer.equals("rock")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". It's a tie!");
                }
                if (computer.equals("paper")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Computer wins!");
                    computerScore++;
                }
                if (computer.equals("scissors")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Human wins!");
                    humanScore++;
                }
            }
            else if (human.equals("paper")){
                if (computer.equals("rock")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Human wins!");
                    humanScore++;
                }
                if (computer.equals("paper")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". It's a tie!");
                }
                if (computer.equals("scissors")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Computer wins!");
                    computerScore++;
                }
            }
            else if (human.equals("scissors")){
                if (computer.equals("rock")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Computer wins!");
                     computerScore++;
                }
                if (computer.equals("paper")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". Human wins!");
                    humanScore++;
                }
                if (computer.equals("scissors")){
                    System.out.println("Human chose "+ human +", computer chose "+ computer+". It's a tie1");  
                }
            
            }
            else {
                System.out.println("I do not understand "+ human + ". Could you try again?");
                continue;
            }
            System.out.println("Score: human "+ humanScore + ", computer "+ computerScore );
            roundCounter++;
            String playAgain = readInput("Do you wish to continue playing? (y/n)?");
            if (playAgain.equals("y")){
                System.out.println("Let's play round "+ roundCounter);
                continue;
            }
            else if(playAgain.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            
        }
            

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
